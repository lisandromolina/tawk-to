export default {
    calculateDays : function(updatedOn) {
        const date_now = Date.now();
        const date_updated = Date.parse(updatedOn);
        const delta = Math.abs(date_now - date_updated) / 1000;
        return Math.floor(delta / 86400);
    }
}