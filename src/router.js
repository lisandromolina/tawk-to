import Vue from 'vue';
import Router from 'vue-router';
import Categories from './components/Categories.vue';
import Knowledgebase from './components/Knowledgebase.vue';
import Search from './components/Search.vue';

import Article from './components/Article.vue';


Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'categories',
            component: Categories
        },
        {
            path: '/knowledgebase/:id',
            name: 'knowledgebase',
            component: Knowledgebase,
            props: true
        },
        {
            path: '/knowledgebase/article/:id',
            name: 'article',
            component: Article,
            props: true
        },
        {
            path: '/search/:keyword',
            name: 'search',
            component: Search,
            props: true
        }
    ]
})