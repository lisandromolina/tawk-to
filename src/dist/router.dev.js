"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _Categories = _interopRequireDefault(require("./components/Categories.vue"));

var _Knowledgebase = _interopRequireDefault(require("./components/Knowledgebase.vue"));

var _Search = _interopRequireDefault(require("./components/Search.vue"));

var _Article = _interopRequireDefault(require("./components/Article.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vueRouter["default"]);

var _default = new _vueRouter["default"]({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'categories',
    component: _Categories["default"]
  }, {
    path: '/knowledgebase/:id',
    name: 'knowledgebase',
    component: _Knowledgebase["default"],
    props: true
  }, {
    path: '/knowledgebase/article/:id',
    name: 'article',
    component: _Article["default"],
    props: true
  }, {
    path: '/search/:keyword',
    name: 'search',
    component: _Search["default"],
    props: true
  }]
});

exports["default"] = _default;