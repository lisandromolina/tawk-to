"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  calculateDays: function calculateDays(updatedOn) {
    var date_now = Date.now();
    var date_updated = Date.parse(updatedOn);
    var delta = Math.abs(date_now - date_updated) / 1000;
    return Math.floor(delta / 86400);
  }
};
exports["default"] = _default;